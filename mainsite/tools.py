from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
from django.core.mail import EmailMessage
from django.conf import settings
from .models import Subscriber
from hashlib import md5

def crop_image(image):
    ASPECT_RATIO = 3.0/2 #Nikon aspect ratio
    THUMBNAIL_HEIGHT = 400
    THUMBNAIL_WIDTH = THUMBNAIL_HEIGHT*ASPECT_RATIO
    img_obj = Image.open(image)
    current_width = img_obj.size[0]
    current_height = img_obj.size[1]
    if (abs(current_width/current_height - ASPECT_RATIO) <= 0.02):
        img_obj.thumbnail((THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT))
    else: 
        resize_ratio = THUMBNAIL_WIDTH/current_width
        new_height = int(current_height*resize_ratio)
        #resize to 600px width
        img_obj = img_obj.resize((int(THUMBNAIL_WIDTH), new_height), Image.ANTIALIAS)
        #crop to 3:2 (starting at upper left corner)
        img_obj = img_obj.crop((0,new_height/2-(THUMBNAIL_HEIGHT/2), THUMBNAIL_WIDTH, new_height/2+(THUMBNAIL_HEIGHT/2)))
    img_io = BytesIO()
    img_obj.save(img_io, format='PNG')
    new_file = ContentFile(img_io.getvalue())
    new_file.name = image.name
    return new_file

def send_message(message, from_name, from_email):
    mail = EmailMessage(subject='Message on dancefaces from {}'.format(from_name),
        body=message, from_email=from_email, to=['dorothea.hoff@web.de'])
    mail.send()

def notify(album):
    subscribers = Subscriber.objects.all()
    for subscriber in subscribers:
        message = '''Hi! 
            I uploaded a new album on dancefaces. 
            Check it out here: http://dhoff.me/dancefaces/albums/{}
            
            If you don't want to receive these messages anymore, please go to 
            http://dhoff.me/dancefaces/unsubscribe/{}_{}'''.format(album.id, subscriber.id, get_user_token(subscriber.id))
        email = EmailMessage(subject='New album: {}'.format(album.title), body=message, 
            from_email='dorothea.hoff@web.de', bcc=[subscriber.email])
        email.send()

def get_user_token(id):
    user = Subscriber.objects.get(id=id)
    secret = settings.SECRET_KEY.encode('utf-8')
    email = user.email.encode('utf-8')
    return md5(email+secret).hexdigest()