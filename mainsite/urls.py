from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('albums/', views.albums, name='albums'),
    path('albums/<int:id>/', views.album, name='album_details'),
    path('albums/<int:id>/edit/', views.album_edit, name='album_edit'),
    path('albums/new/', views.album_new, name='album_new'),
    path('contact/', views.contact, name='contact'),
    path('share/', views.share, name='share'),
    path('unsubscribe/<int:id>_<token>/', views.unsubscribe, name='unsubscribe')
]