from django import forms
from .models import Album, Picture, Subscriber
from django.forms.widgets import ClearableFileInput
from django.utils.safestring import mark_safe
from .tools import send_message

class ThumbnailPreview(ClearableFileInput):
    def __init__(self, attrs=None):
        super(ThumbnailPreview, self).__init__(attrs)
    def render(self, name, value, attrs=None):
        if (value and getattr(value, 'url', None)):
            return mark_safe('<img src="{}"/>'.format(value.url))

class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ('title', 'timestamp')

class PictureForm(forms.ModelForm):
    to_delete = forms.BooleanField(required=False)
    class Meta:
        model = Picture
        widgets = {
            'thumbnail': ThumbnailPreview
        }
        fields = ('is_starred', 'thumbnail', 'id')

    #def save(self, commit=True):
    #    return super(PictureForm, self).save(commit=commit)

PictureFormSet = forms.modelformset_factory(Picture, form=PictureForm, extra=0)

class ContactForm(forms.ModelForm):
    subscribe = forms.BooleanField(required=False)
    message = forms.CharField(widget=forms.Textarea, required=False)
    class Meta:
        model = Subscriber
        fields = ('name', 'email')

    def save(self, commit=True):
        subscribe = self.cleaned_data.get('subscribe', None)
        message = self.cleaned_data.get('message', None)
        name = self.cleaned_data.get('name', None)
        email = self.cleaned_data.get('email', None)
        if (message):
            send_message(message, name, email)
        if (subscribe == True):
            return super(ContactForm, self).save(commit=commit)
        