from django.db import models
from datetime import date

class Album(models.Model):
    title = models.CharField(max_length=200)
    timestamp = models.DateField(default=date.today)
    preview = models.ForeignKey('Picture', on_delete=models.SET_NULL, blank=True, null=True, 
    related_name='preview_for')

    def __str__(self):
        return self.title

def album_path(picture_instance, filename):
    return 'album_{0}/{1}'.format(picture_instance.album.id, filename)

def preview_path(picture_instance, filename):
    return 'album_{0}/prev/{1}'.format(picture_instance.album.id, filename)

class Picture(models.Model):
    file = models.ImageField(upload_to=album_path)
    thumbnail = models.ImageField(upload_to=preview_path)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    is_starred = models.BooleanField()

class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        return self.name
