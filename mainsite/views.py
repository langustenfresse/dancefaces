from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Album, Picture, Subscriber
from random import randint, random
from .forms import AlbumForm, PictureFormSet, ContactForm
from .tools import crop_image, notify, get_user_token

def home(request):
    featured_pictures = Picture.objects.filter(is_starred=True)
    featured = list(sorted(featured_pictures, key=lambda x: random()))
    featured = featured[0:4]
    return render(request, 'dancefaces/index.html', {'featured': featured})

def albums(request):
    albums = Album.objects.all().order_by('timestamp')
    albums_with_preview = []
    for album in albums:
        prev_candidates = Picture.objects.filter(album=album.id, is_starred=True)
        if (len(prev_candidates) == 0):
            prev_candidates = Picture.objects.filter(album=album.id)
        try:
            index = randint(0, len(prev_candidates)-1)
        except ValueError:
            index = 0
        try:
            album.preview = prev_candidates[index]
            albums_with_preview.append(album)
        except IndexError:
            album.preview = None
    return render(request, 'dancefaces/album_list.html', {'albums': albums_with_preview})

def album(request, id):
    album = Album.objects.get(id=id)
    pictures = Picture.objects.filter(album=id)
    return render(request, 'dancefaces/album_detail.html', {'pictures': pictures, 'album': album})

@login_required
def album_new(request):
    if (request.method == 'POST'):
        form = AlbumForm(request.POST)
        if form.is_valid():
            album = form.save()
            for uploaded in request.FILES.getlist('pictures'):
                thumb = crop_image(uploaded)
                print(thumb)
                picture = Picture(album=album, file=uploaded, thumbnail=thumb, is_starred=False)
                picture.save()
            notify(album)
            return redirect('albums')
    else:
        form = AlbumForm()
        return render(request, 'dancefaces/album_form.html', {'form': form})

@login_required
def album_edit(request, id):
    if (request.method == 'POST'):
        edited = PictureFormSet(request.POST)
        if edited.is_valid():
            for edit in edited:
                pic = edit.save(commit=False)
                if (edit.cleaned_data.get('to_delete', None)):
                    pic.delete()
                else:
                    pic.save()
        return redirect('home')
    else:
        album = Album.objects.get(id=id)
        pictures = Picture.objects.filter(album=id)
        form = PictureFormSet(queryset=pictures)
        return render(request, 'dancefaces/album_edit.html', {'form': form, 'album': album})

def contact(request):
    if (request.method == 'POST'):
        form = ContactForm(request.POST)
        if form.is_valid():
            data = form.save()
            reply = ""
            subscribe = form.cleaned_data.get('subscribe')
            if subscribe:
                reply += "From now on, you'll get an email whenever I upload a new album."
            msg = form.cleaned_data.get('message')
            if msg:
                reply += "I'll get back to your message as soon as possible."
        return render(request, 'dancefaces/message.html', {'heading': 'Thanks!', 'message': reply})
    else:
        form = ContactForm()
        return render(request, 'dancefaces/contact.html', {'form': form})

def share(request):
    return render(request, 'dancefaces/share.html')

def unsubscribe(request, id, token):
    try:
        if (token == get_user_token(id)):
            subscriber = Subscriber.objects.get(id=id)
            name = subscriber.name
            subscriber.delete()
            msg = "Thanks {}, you will no longer receive email updates.".format(name)
        else:
            msg = "You don't seem to be subscribed here."
        return render(request, 'dancefaces/message.html', {'heading': "Unsubscribe", 'message': msg})
    except Subscriber.DoesNotExist:
        msg = "You don't seem to be subscribed here."
        return render(request, 'dancefaces/message.html', {'heading': "Unsubscribe", 'message': msg})