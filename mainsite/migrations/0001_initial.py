# Generated by Django 2.0.1 on 2018-01-06 18:04

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import mainsite.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to=mainsite.models.album_path)),
                ('is_starred', models.BooleanField()),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainsite.Album')),
            ],
        ),
    ]
