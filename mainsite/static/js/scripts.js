const header = document.getElementById('header');
window.onscroll = () => {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
         header.classList.remove('nav-down');
         header.classList.add('nav-up');
     }

    else {
        header.classList.remove('nav-up');
        header.classList.add('nav-down');
     }
 }

const gallery = document.getElementById('gallery');
const viewBox = document.getElementById('gallery-display');
 
function toggleGallery(imagePath = '') {
    const display = gallery.style.display;;

    if (display === 'none') {
        viewBox.setAttribute('src', imagePath);
        gallery.setAttribute('style', 'display: flex');
        return;
    }

    gallery.setAttribute('style', 'display: none');
    viewBox.setAttribute('src', '');
 }

gallery.addEventListener('click', toggleGallery);

const galleryToggles = document.getElementsByClassName('gallery-toggle');
for (i = 0; i < galleryToggles.length; i++) {
    const imagePath = galleryToggles[i].getAttribute('href');
    
    galleryToggles[i].addEventListener('click', function (event) {
        event.preventDefault();
        toggleGallery(imagePath);
    })
} 